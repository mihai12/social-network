package com.example.proiect_extins.service.entityservice;

import com.example.proiect_extins.domain.EventDTO;
import com.example.proiect_extins.domain.NotificationEventDTO;
import com.example.proiect_extins.domain.User;
import com.example.proiect_extins.domain.UserDTO;
import com.example.proiect_extins.repository.db.EventDb;

import java.time.LocalDate;
import java.util.List;

public class EventService {
    EventDb repoEvent;

    public EventService(EventDb repoEvent){
        this.repoEvent = repoEvent;
    }

    public EventDTO findOne(Integer eventId){
        return repoEvent.findOne(eventId);
    }

    public Iterable<EventDTO> findAll(){
        return repoEvent.findAll();
    }

    public EventDTO save(EventDTO event){
        return repoEvent.save(event);
    }

    public EventDTO remove(Integer eventId){
        return repoEvent.remove(eventId);
    }

    public int size(){
        return repoEvent.size();
    }

    public UserDTO addParticipant(UserDTO user, int eventId){
        return repoEvent.addParticipant(user, eventId);
    }

    public void removeParticipant(String email, int eventId){
        repoEvent.removeParticipant(email, eventId);
    }

    public int noParticipants(int eventId) {
        return repoEvent.noParticipants(eventId);
    }

    public boolean userParticipate(String id, Integer id1) {
        return repoEvent.userParticipate(id, id1);
    }

    public void turnOnNotificationEvent(String id, Integer id1) {
        repoEvent.turnOnNotificationEvent(id, id1);
    }

    public void turnOffNotificationEvent(String id, Integer id1) {
        repoEvent.turnOffNotificationEvent(id, id1);
    }

    public boolean getStatusNotifications(String id, Integer id1) {
        return repoEvent.getStatusNotifications(id, id1);
    }

    public List<NotificationEventDTO> getNotificationsEvents(String id, LocalDate now) {
        return repoEvent.getNotificationsEvents(id, now);
    }

    public EventDTO getEventByPosition(int currentEvent) {
        return repoEvent.getEventByPosition(currentEvent);
    }
}
