package com.example.proiect_extins.service.validators;


public class NonExistingException extends RuntimeException{

    public NonExistingException(String msj){
        super(msj);
    }
}
