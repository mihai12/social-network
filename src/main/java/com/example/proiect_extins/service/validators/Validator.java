package com.example.proiect_extins.service.validators;


public interface Validator<T> {
    void validate(T entity) throws ValidationException;

}