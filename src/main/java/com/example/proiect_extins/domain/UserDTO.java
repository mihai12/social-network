package com.example.proiect_extins.domain;

public class UserDTO extends Entity<String> {
    String id;
    String firstName;
    String lastName;

    public UserDTO(User user){

        this.id = user.getId();
        this.firstName = user.getFirstName();
        this.lastName = user.getLastName();
    }

    public UserDTO(String email, String first_name, String last_name) {
        this.id = email;
        this.firstName = first_name;
        this.lastName = last_name;
    }

    /**
     *
     * @return the id to a user
     */
    public String getId(){
        return id;
    }

    /**
     *
     * @return the firstName to a user
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     *
     * @return  the lastName to a user
     */
    public String getLastName(){
        return lastName;
    }

    @Override
    public String toString(){
        return  id + " : " + firstName + ", " + lastName;
    }
}
