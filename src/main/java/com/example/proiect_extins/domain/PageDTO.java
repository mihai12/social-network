package com.example.proiect_extins.domain;

import java.util.List;

public class PageDTO {
    UserDTO loggedUser;
    List<NotificationEventDTO> notifications;

    public UserDTO getLoggedUser() {
        return loggedUser;
    }

    public void setLoggedUser(UserDTO loggedUser) {
        this.loggedUser = loggedUser;
    }

    public List<NotificationEventDTO> getNotifications() {
        return notifications;
    }

    public void setNotifications(List<NotificationEventDTO> notifications) {
        this.notifications = notifications;
    }
}
