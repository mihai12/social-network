package com.example.proiect_extins.domain;

import java.time.LocalDate;

public class NotificationEventDTO {
    int id;
    String name;
    String location;
    String description;
    LocalDate date;
    int period;

    public NotificationEventDTO(int id, String name, String location, String description, LocalDate date, int period) {
        this.id = id;
        this.name = name;
        this.location = location;
        this.description = description;
        this.date = date;
        this.period = period;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getLocation() {
        return location;
    }

    public String getDescription() {
        return description;
    }

    public LocalDate getDate() {
        return date;
    }

    public int getPeriod() {
        return period;
    }
}
