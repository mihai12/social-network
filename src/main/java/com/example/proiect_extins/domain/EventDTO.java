package com.example.proiect_extins.domain;

import java.time.LocalDate;
import java.util.List;

public class EventDTO extends Entity<Integer>{
    private String name;
    private String description;
    private String location;
    private LocalDate date;
    private List<UserDTO> participants;

    public EventDTO(String name, String description, String location, LocalDate date, List<UserDTO> participants) {
        this.name = name;
        this.description = description;
        this.location = location;
        this.date = date;
        this.participants = participants;
    }

    public String getName() {
        return this.name;
    }

    public String getDescription() {
        return this.description;
    }

    public String getLocation() {
        return this.location;
    }

    public LocalDate getDate() {
        return this.date;
    }

    public List<UserDTO> getParticipants() {
        return this.participants;
    }
}
