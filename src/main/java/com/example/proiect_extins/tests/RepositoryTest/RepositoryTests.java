package com.example.proiect_extins.tests.RepositoryTest;


import com.example.proiect_extins.tests.RepositoryTest.RepoDBTest.FriendshipDBTest;
import com.example.proiect_extins.tests.RepositoryTest.RepoDBTest.MessageDBTest;
import com.example.proiect_extins.tests.RepositoryTest.RepoDBTest.UserDBTest;

public class RepositoryTests {

    public static void runTests(){
        UserDBTest.runTests();
        FriendshipDBTest.runTests();
        MessageDBTest.runTests();
    }
}
