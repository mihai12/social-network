package com.example.proiect_extins.tests;


import com.example.proiect_extins.tests.RepositoryTest.RepoDBTest.FriendshipRequestDBTest;
import com.example.proiect_extins.tests.RepositoryTest.RepositoryTests;
import com.example.proiect_extins.tests.DomainTests.DomainTests;


public class Tests {

    public static void RunALL() {
        RepositoryTests.runTests();
        DomainTests.runTest();
        ServiceTests.runTests();
        FriendshipRequestDBTest.runTests();
    }
}
