package com.example.proiect_extins.domainEvent;

import javafx.event.Event;
import javafx.event.EventType;

public class LoadEventsViews extends Event {
    private static final long serialVersionUID = 97924780751776547L;

    public static final EventType<LoadEventsViews> VIEW = new EventType<>("VIEW");
    public static final EventType<LoadEventsViews> ANY = VIEW;
    public static final EventType<LoadEventsViews> SIGN_UP_NEXT = new EventType<>(LoadEventsViews.ANY,"SIGN_UP_NEXT");
    public static final EventType<LoadEventsViews> FINAL_SIGN_UP = new EventType<>(LoadEventsViews.ANY,"FINAL_SIGN_UP");
    public static final EventType<LoadEventsViews> LOAD_EVENTS = new EventType<>(LoadEventsViews.ANY,"LOAD_EVENTS");

    public LoadEventsViews(EventType<? extends Event> eventType) {
        super(eventType);
    }
}
