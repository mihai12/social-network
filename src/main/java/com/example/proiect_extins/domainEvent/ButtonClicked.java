package com.example.proiect_extins.domainEvent;

import javafx.event.Event;
import javafx.event.EventType;

public class ButtonClicked extends Event {
    private static final long serialVersionUID = 325256635623L;

    public static final EventType<ButtonClicked> BUTTON = new EventType<>("BUTTON");
    public static final EventType<ButtonClicked> ANY = BUTTON;

    public ButtonClicked(EventType<? extends Event> eventType) {
        super(eventType);
    }
}
