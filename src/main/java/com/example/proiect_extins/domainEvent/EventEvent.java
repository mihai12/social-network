package com.example.proiect_extins.domainEvent;

import javafx.event.Event;
import javafx.event.EventTarget;
import javafx.event.EventType;

public class EventEvent extends Event {
    private static final long serialVersionUID = 97924780751776547L;
    public static final EventType<EventEvent> VIEW = new EventType<>("CURSOR");
    public static final EventType<EventEvent> ANY = VIEW;
    public static final EventType<EventEvent> NEXT_EVENT = new EventType<>(EventEvent.ANY,"NEXT_EVENT");
    public static final EventType<EventEvent> PREVIOUS_EVENT = new EventType<>(EventEvent.ANY,"PREVIOUS_EVENT");

    public EventEvent(EventType<? extends javafx.event.Event> eventType) {
        super(eventType);
    }

    public EventEvent(Object source, EventTarget target, EventType<? extends javafx.event.Event> eventType) {
        super(source, target, eventType);
    }
}
