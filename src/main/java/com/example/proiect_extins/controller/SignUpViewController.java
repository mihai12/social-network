package com.example.proiect_extins.controller;

import com.example.proiect_extins.domain.User;
import com.example.proiect_extins.domainEvent.ButtonClicked;
import com.example.proiect_extins.service.SuperService;
import com.example.proiect_extins.service.validators.ValidationException;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;

public class SignUpViewController {
    SuperService service;

    @FXML
    private TextField passwordText;

    @FXML
    private TextField usernameTxt;

    @FXML
    private TextField firstNameText;

    @FXML
    private TextField lastNameText;

    @FXML Button logInBtn;

    public void setService(SuperService service) {
        this.service = service;
    }

    public void handlerButtonPressed(){
        if(!usernameTxt.getText().equals("") && !passwordText.getText().equals("") && !firstNameText.getText().equals("") && !lastNameText.getText().equals("")) {
            try {
                service.addUser(new User(firstNameText.getText(), lastNameText.getText(), usernameTxt.getText(), passwordText.getText()));
            }catch (ValidationException e){
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setContentText(e.getMessage());
                alert.show();
            }
        }
        else{
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText("All textFields must be completed!!!");
            alert.show();
        }
    logInBtn.fireEvent(new ButtonClicked(ButtonClicked.BUTTON));
    }

    @FXML
    public void handlerExitBtnPressed(){
        logInBtn.fireEvent(new ButtonClicked(ButtonClicked.BUTTON));
    }
}
