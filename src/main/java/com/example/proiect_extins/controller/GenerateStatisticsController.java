package com.example.proiect_extins.controller;

import com.example.proiect_extins.LogInApplication;
import com.example.proiect_extins.domain.*;
import com.example.proiect_extins.domainEvent.ItemSelected;
import com.example.proiect_extins.service.SuperService;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Alert;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.stage.DirectoryChooser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDType1Font;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class GenerateStatisticsController {

    SuperService service;
    UserDTO loggedUser;
    String emailFriend="";

    @FXML
    private TextField pathTextField;

    @FXML
    private DatePicker beginDate;

    @FXML
    private DatePicker endDate;

    @FXML
    private TextField friendText;

    @FXML
    private GridPane gridPane;

    private String address="";

    public void load(SuperService service, UserDTO loggedUser){
        this.service = service;
        this.loggedUser = loggedUser;
    }


    @FXML
    private void handlerGeneratePath(){
        DirectoryChooser chooser = new DirectoryChooser();
        File selectedFile = chooser.showDialog(null);
        if(selectedFile != null){
            address = selectedFile.toString();
        }
        pathTextField.setText(address);
    }

    private Pane createItem(UserDTO user) throws IOException{
        FXMLLoader loader = new FXMLLoader(LogInApplication.class.getResource("searchFriendItem.fxml"));
        Pane item = loader.load();
        SearchFriendItemController controller= loader.getController();
        controller.setUser(user);
        return item;
    }

    @FXML
    private void handlerWriting() throws IOException {
        gridPane.getChildren().clear();
        List<UserDTO> users = new ArrayList<>();
        if (friendText.getText() != "") {
            users = service.findAllByString(friendText.getText(), loggedUser.getId());
        }
        for (UserDTO user : users){
            Pane item = createItem(user);
            item.addEventFilter(ItemSelected.USER_SELECTED, this::handlerFriendClicked);
            gridPane.addRow(gridPane.getRowCount(), item);
        }
    }

    private void handlerFriendClicked(ItemSelected item){
        emailFriend = item.getSelectedItemId();
        User friend = service.findOneUser(emailFriend);
        friendText.setText(friend.getFirstName()+" "+friend.getLastName());
    }

    @FXML
    private void handlerGeneratePDF2() {
        try {
            if (beginDate.getValue().toString() != "" && endDate.getValue().toString() != "") {
                if(emailFriend.equals("")){
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setContentText("Friend name is not valid!");
                    alert.show();
                }
                else {
                    User friend = service.findOneUser(emailFriend);
                    List<MessageDTO> messages = service.getMessagesBetweenFriends(beginDate.getValue(), endDate.getValue(), loggedUser.getId(), emailFriend);
                    String fileName = "/Messages.pdf";
                    PDDocument doc = new PDDocument();
                    PDPage page = new PDPage();
                    doc.addPage(page);
                    try {
                        PDPageContentStream content = new PDPageContentStream(doc, page);
                        content.setFont(PDType1Font.COURIER, 12);
                        float tx = 50;
                        float ty = 700;

                        content.beginText();
                        content.moveTextPositionByAmount(tx, ty);
                        content.setFont(PDType1Font.COURIER_BOLD, 12);
                        content.drawString("Messages from friend " + friend.getFirstName() + " " + friend.getLastName() + " between " + beginDate.getValue() + " and " + endDate.getValue() + " : ");
                        ty -= 24;
                        content.endText();
                        if (messages.size() == 0) {
                            content.beginText();
                            content.moveTextPositionByAmount(70, ty);
                            content.setFont(PDType1Font.COURIER, 12);
                            content.drawString("Does not exist received messages in this period !!!");
                            ty -= 24;
                            content.endText();
                        }
                        for (MessageDTO message : messages) {
                            content.beginText();
                            content.moveTextPositionByAmount(tx, ty);
                            User user = service.findOneUser(message.getFrom());
                            content.setFont(PDType1Font.COURIER, 12);
                            content.drawString(message.getMessage());
                            content.endText();
                            ty -= 12;
                            content.beginText();
                            content.moveTextPositionByAmount(tx, ty);
                            content.drawString(message.getData().toString());
                            //content.showText(message.getMessage()+message.getFrom()+message.getData());
                            ty -= 24;
                            content.endText();
                        }

                        if (address != "") {
                            content.close();
                            doc.save(address + fileName);
                        } else {
                            Alert alert = new Alert(Alert.AlertType.ERROR);
                            alert.setContentText("Choose the directory!");
                            alert.show();
                        }
                        doc.close();

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            } else {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setContentText("Date textFields must not be empty!");
                alert.show();
            }
        }catch (NullPointerException e){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText("All textFields must not be empty!");
            alert.show();
        }
    }

    @FXML
    private void handlerGeneratePDF1() throws IOException {
        try {
            if (beginDate.getValue().toString() != "" && endDate.getValue().toString() != "") {
                List<MessageDTO> messages = service.getMessagesBetween(beginDate.getValue(), endDate.getValue(), loggedUser);
                List<FriendShipDTO> friendships = service.getFriendshipsBetween(beginDate.getValue(), endDate.getValue(), loggedUser.getId());
                String fileName = "/Statistics.pdf";
                PDDocument doc = new PDDocument();
                PDPage page = new PDPage();
                doc.addPage(page);
                PDPageContentStream content = new PDPageContentStream(doc, page);

                content.setFont(PDType1Font.COURIER, 12);
                float tx = 50;
                float ty = 700;

                try {
                    content.beginText();
                    content.moveTextPositionByAmount(tx, ty);
                    content.setFont(PDType1Font.COURIER_BOLD, 12);
                    content.drawString("Messages between " + beginDate.getValue() + " and " + endDate.getValue() + " : ");
                    ty -= 24;
                    content.endText();
                    if (messages.size() == 0) {
                        content.beginText();
                        content.moveTextPositionByAmount(70, ty);
                        content.setFont(PDType1Font.COURIER, 12);
                        content.drawString("Does not exist received messages in this period!!!");
                        ty -= 24;
                        content.endText();
                    }
                    for (MessageDTO message : messages) {
                        content.beginText();
                        content.moveTextPositionByAmount(tx, ty);
                        User user = service.findOneUser(message.getFrom());
                        content.setFont(PDType1Font.COURIER, 12);
                        content.drawString(message.getMessage() + "| From " + user.getFirstName() + " " + user.getLastName());
                        content.endText();
                        ty -= 12;
                        content.beginText();
                        content.moveTextPositionByAmount(tx, ty);
                        content.drawString(message.getData().toString());
                        //content.showText(message.getMessage()+message.getFrom()+message.getData());
                        ty -= 24;
                        content.endText();
                    }

                    content.beginText();
                    content.moveTextPositionByAmount(tx, ty);
                    content.setFont(PDType1Font.COURIER_BOLD, 12);
                    content.drawString("Friendships created between " + beginDate.getValue() + " and " + endDate.getValue() + " : ");
                    ty -= 24;
                    content.endText();

                    if (friendships.size() == 0) {
                        content.beginText();
                        content.moveTextPositionByAmount(tx, ty);
                        content.setFont(PDType1Font.COURIER, 12);
                        content.drawString("Between " + beginDate.getValue() + " and " + endDate.getValue() + " no friendships have been created!");
                        ty -= 24;
                    }

                    for (FriendShipDTO friendShipDTO : friendships) {
                        content.beginText();
                        content.moveTextPositionByAmount(tx, ty);
                        content.setFont(PDType1Font.COURIER, 12);
                        content.drawString(friendShipDTO.getUser1().getFirstName() + " " + friendShipDTO.getUser1().getLastName() + " " + friendShipDTO.getDate());
                        content.endText();
                        ty -= 12;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

                if(address != "") {
                    content.close();
                    doc.save(address + fileName);
                }
                else{
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setContentText("Choose the directory!");
                    alert.show();
                }
                doc.close();
            }
        }catch(NullPointerException e){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText("Date text fields must not be empty!");
            alert.show();
        }
    }
}
