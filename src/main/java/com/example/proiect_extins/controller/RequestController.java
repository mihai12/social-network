package com.example.proiect_extins.controller;

import com.example.proiect_extins.LogInApplication;
import com.example.proiect_extins.domain.FriendShipDTO;
import com.example.proiect_extins.domain.User;
import com.example.proiect_extins.domain.UserDTO;
import com.example.proiect_extins.service.SuperService;
import com.example.proiect_extins.service.validators.NonExistingException;
import com.example.proiect_extins.utils.events.ViewItemEvent;
import com.example.proiect_extins.utils.observer.Observer;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class RequestController implements Observer<ViewItemEvent> {
    UserDTO loggedUser;
    SuperService service;

    @FXML
    private GridPane gridPane;

    private List<UserDTO> requests;

    @FXML
    private ImageView noRequestsIcon;

    @FXML
    private Label noRequestsText;

    public void setLoggedUser(UserDTO loggedUser) {
        this.loggedUser = loggedUser;
    }

    public void setService(SuperService service) {
        service.addObserver(this);
        this.service = service;
    }

    private Pane createItem(FriendShipDTO request) throws IOException{
        FXMLLoader loader = new FXMLLoader(LogInApplication.class.getResource("request-item.fxml"));
        Pane item = loader.load();
        RequestItemController requestItemController = loader.getController();
        requestItemController.setName(request.getUser2().getFirstName()+" "+request.getUser2().getLastName());
        requestItemController.setDate(request.getDate());
        requestItemController.setLoggedUser(loggedUser);
        requestItemController.setService(service);
        requestItemController.setEmail(request.getUser2().getId());
        return item;
    }

    public void load(){
        try {
            requests=new ArrayList<>();
            List<FriendShipDTO> friendsList = (List<FriendShipDTO>) service.getFriendRequest(loggedUser.getId());
            if(friendsList.size()!=0){
                noRequestsText.setVisible(false);
                noRequestsIcon.setVisible(false);
            }
            service.getFriendRequest(loggedUser.getId()).forEach(friendShipDTO-> {
                try {
                    Pane item = createItem(friendShipDTO);
                    gridPane.addRow(gridPane.getRowCount(), item);
                    requests.add(friendShipDTO.getUser2());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        }catch (NonExistingException e){
            System.out.println(e.getMessage());
        }
    }

    @Override
    public void update(ViewItemEvent viewItemEvent) {
        gridPane.getChildren().remove(requests.indexOf(viewItemEvent.getUserDTO()));
        requests.remove(viewItemEvent.getUserDTO());
    }
}
