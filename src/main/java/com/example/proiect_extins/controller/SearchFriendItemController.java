package com.example.proiect_extins.controller;

import com.example.proiect_extins.domain.UserDTO;
import com.example.proiect_extins.domainEvent.ItemSelected;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;

public class SearchFriendItemController {
    private String first_name;
    private String last_name;
    private String email;

    @FXML
    private Label friendName;

    @FXML
    private AnchorPane pane;

    public void setUser(UserDTO user){
        this.email= user.getId();
        this.first_name = user.getFirstName();
        this.last_name = user.getLastName();
        friendName.setText(first_name+" "+last_name);
    }

    @FXML
    public void handlerFriendClicked(){
        pane.fireEvent(new ItemSelected(ItemSelected.USER_SELECTED, email));
    }
}
