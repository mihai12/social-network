package com.example.proiect_extins.controller;

import com.example.proiect_extins.LogInApplication;
import com.example.proiect_extins.domain.*;
import com.example.proiect_extins.service.SuperService;
import com.example.proiect_extins.service.validators.ValidationException;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;

import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

public class MainWindowController {

    @FXML
    private TextField searchbar;

    @FXML
    private BorderPane borderPane;

    @FXML
    private Label loggedUserFirstName;

    @FXML
    private Label loggedUserLastName;

    @FXML
    private AnchorPane notificationsPane;

    @FXML
    private GridPane gridPane;

    @FXML
    private ScrollPane scrollPaneNotifications;

    @FXML
    private Label labelNotifications;

    @FXML
    private Label text1;

    @FXML
    private Label text2;

    @FXML
    private AnchorPane anchorNotifications;

    private SuperService service;

    private PageDTO page = new PageDTO();

    public void load(SuperService service, User loggedUser) throws IOException{
        this.service=service;
        borderPane.getLeft().setVisible(false);
        page.setLoggedUser(new UserDTO(loggedUser));
        page.setNotifications(service.getNotificationsEvents(page.getLoggedUser().getId(), LocalDate.now()));
        loggedUserFirstName.setText(page.getLoggedUser().getFirstName());
        loggedUserLastName.setText(page.getLoggedUser().getLastName());
        handlerShowNotifications();
    }

    private void handlerShowNotifications() throws IOException{
        List<NotificationEventDTO> notifications = page.getNotifications();
        if(notifications.size()==0){
            scrollPaneNotifications.setVisible(false);
        }
        else{
            text1.setVisible(false);
            text2.setVisible(false);
            for(NotificationEventDTO notification : notifications) {
                FXMLLoader loader = new FXMLLoader(LogInApplication.class.getResource("notificationItem.fxml"));
                Pane item = loader.load();
                NotificationItemController notificationItemController = loader.getController();
                notificationItemController.setItem(notification);
                gridPane.addRow(gridPane.getRowCount(), item);
                gridPane.addRow(gridPane.getRowCount(), new Text(""));
            }
        }
        scrollPaneNotifications.setVisible(true);
    }

    @FXML
    private void handlerGenerateStatisticsBtn() throws IOException {
        FXMLLoader loader = new FXMLLoader(LogInApplication.class.getResource("statisticsView.fxml"));
        Pane pane = loader.load();
        GenerateStatisticsController statisticsControllerController= loader.getController();
        borderPane.setCenter(pane);
        statisticsControllerController.load(service, page.getLoggedUser());
    }

    @FXML
    private void handlerFriendsButton() throws IOException {
        FXMLLoader loader = new FXMLLoader(LogInApplication.class.getResource("friends-view.fxml"));
        Pane pane = loader.load();
        FriendsController friendsController= loader.getController();
        friendsController.load(service,page.getLoggedUser());
        borderPane.setCenter(pane);
    }

    @FXML
    private void handlerKeyPressed(KeyEvent keyEvent) throws IOException {
        if (keyEvent.getCode().equals(KeyCode.ENTER))
        {
            FXMLLoader fxmlLoader = new FXMLLoader(LogInApplication.class.getResource("search-view.fxml"));
            Pane view = fxmlLoader.load();
            SearchController searchController = fxmlLoader.getController();
            searchController.setService(service);
            searchController.setLoggedUser(page.getLoggedUser());
            try{searchController.load(searchbar.getText());} catch (ValidationException ignored){}
            borderPane.setCenter(view);
        }
    }

    @FXML
    private void handlerRequestsButton() throws IOException{
        FXMLLoader loader= new FXMLLoader(LogInApplication.class.getResource("request-view.fxml"));
        AnchorPane panel= loader.load();

        RequestController requestController= loader.getController();
        requestController.setService(service);
        requestController.setLoggedUser(page.getLoggedUser());
        requestController.load();

        Pane view = new Pane(panel);
        borderPane.getLeft().setVisible(false);
        borderPane.getLeft().prefWidth(0);
        borderPane.setCenter(view);
        borderPane.getCenter().prefWidth(700);
    }

    @FXML
    private void handlerMessageBtn() throws IOException {
        FXMLLoader loader= new FXMLLoader(LogInApplication.class.getResource("messageWindow-view.fxml"));
        Pane panel= loader.load();
        MessageController messageController=loader.getController();
        messageController.load(service,page.getLoggedUser());
        borderPane.setCenter(panel);
    }

    @FXML
    public void handlerExitBtnMW(Event actionEvent) {
        ((Stage)(((ImageView)actionEvent.getSource()).getScene().getWindow())).close();
    }

    @FXML
    public void handlerMinimizeBtnMW(Event actionEvent) {
        ((Stage)(((ImageView)actionEvent.getSource()).getScene().getWindow())).setIconified(true);
    }

    @FXML
    public void handlerExtindBtnMW(Event actionEvent) {
        ((Stage)(((ImageView)actionEvent.getSource()).getScene().getWindow())).setFullScreen(true);
    }

    @FXML
    private void handlerLogOutBtn() throws IOException {

        FXMLLoader fxmlLoader = new FXMLLoader(LogInApplication.class.getResource("logIn-view.fxml"));

        AnchorPane panel= fxmlLoader.load();

        LogInController logInController= fxmlLoader.getController();
        logInController.setService(service);

        Stage stage= new Stage();
        Scene scene = new Scene(panel, 580, 460);
        scene.getStylesheets().add(LogInApplication.class.getResource("log.css").toExternalForm());
        scene.setFill(Color.TRANSPARENT);
        stage.initStyle(StageStyle.TRANSPARENT);
        stage.setScene(scene);
        stage.show();

        Stage mainWnd =(Stage) borderPane.getScene().getWindow();
        mainWnd.close();
    }

    @FXML
    private void handlerEventBtn() throws IOException{
        FXMLLoader fxmlLoader = new FXMLLoader(LogInApplication.class.getResource("eventView.fxml"));
        AnchorPane pane = fxmlLoader.load();
        EventController eventController = fxmlLoader.getController();
        eventController.setService(service);
        eventController.setLoggedUser(page.getLoggedUser());
        borderPane.setCenter(pane);

    }

    @FXML
    private void handlerIconSelected(){
        if(!borderPane.getLeft().isVisible()) {
            borderPane.getLeft().setVisible(true);
            scrollPaneNotifications.setVisible(false);
            labelNotifications.setVisible(false);
            anchorNotifications.setVisible(false);
        }
        else{
            borderPane.getLeft().setVisible(false);
            scrollPaneNotifications.setVisible(true);
            labelNotifications.setVisible(true);
            anchorNotifications.setVisible(true);
        }
    }
}
