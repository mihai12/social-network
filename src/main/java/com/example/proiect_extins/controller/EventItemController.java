package com.example.proiect_extins.controller;

import com.example.proiect_extins.domain.EventDTO;
import com.example.proiect_extins.domain.User;
import com.example.proiect_extins.domain.UserDTO;
import com.example.proiect_extins.domainEvent.EventEvent;
import com.example.proiect_extins.domainEvent.LoadEventsViews;
import com.example.proiect_extins.service.SuperService;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;

public class EventItemController {
    @FXML
    private Label titleEventLabel, numberOfParticipantsLabel, timeEventLabel, locationEventLabel, descriptionEventLabel;

    @FXML
    private ImageView prevEventImageView;

    @FXML
    private ImageView nextEventImageView;

    @FXML
    private CheckBox notificationCheckBox;

    @FXML
    private CheckBox participatingCheckBox;

    @FXML
    Button removeEventBtn;

    private EventDTO eventDTO;
    private SuperService service;
    private UserDTO loggedUser;

    @FXML
    public void handlerNextEvent() {
        nextEventImageView.getParent().fireEvent(new EventEvent(EventEvent.NEXT_EVENT));
    }

    @FXML
    public void handlerPreviousEvent() {
        prevEventImageView.getParent().fireEvent(new EventEvent(EventEvent.PREVIOUS_EVENT));
    }

    public void load(EventDTO eventDTO) {
        this.eventDTO = eventDTO;
        titleEventLabel.setText(eventDTO.getName());
        numberOfParticipantsLabel.setText(String.valueOf(0));
        timeEventLabel.setText(eventDTO.getDate().toString());
        locationEventLabel.setText(eventDTO.getLocation());
        descriptionEventLabel.setText(eventDTO.getDescription());
        numberOfParticipantsLabel.setText(String.valueOf(service.noParticipants(eventDTO.getId())));
        participatingCheckBox.setSelected(service.userParticipate(loggedUser.getId(), eventDTO.getId()));
        notificationCheckBox.setSelected(service.getStatusNotifications(loggedUser.getId(), eventDTO.getId()));
    }

    public void setService(SuperService service){
        this.service = service;
    }

    public void setLoggedUser(UserDTO loggedUser) {
        this.loggedUser = loggedUser;
    }

    @FXML
    public void handlerParticipatingCheckBox() {
        if (participatingCheckBox.isSelected()){
            service.addParticipant(loggedUser, eventDTO.getId());
        }
        else if (! participatingCheckBox.isSelected()){
            service.removeParticipant(loggedUser.getId(), eventDTO.getId());
        }
    }

    @FXML
    public void handlerNotificationOnCheckBox() {
        if (notificationCheckBox.isSelected()) {
            service.turnOnNotificationEvent(loggedUser.getId(), eventDTO.getId());
        }
        else {
            service.turnOffNotificationEvent(loggedUser.getId(), eventDTO.getId());
        }
    }

    public void handlerRemoveEvent() {
        service.removeEvent(eventDTO.getId());
        removeEventBtn.fireEvent(new LoadEventsViews(LoadEventsViews.LOAD_EVENTS));
    }
}
