package com.example.proiect_extins.controller;

import com.example.proiect_extins.LogInApplication;
import com.example.proiect_extins.domain.UserDTO;
import com.example.proiect_extins.domainEvent.EventEvent;
import com.example.proiect_extins.domainEvent.LoadEventsViews;
import com.example.proiect_extins.service.SuperService;

import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;

import java.io.IOException;

public class EventController {

    @FXML
    private BorderPane borderPane;

    private SuperService service;
    private int currentEvent = 0;
    private UserDTO loggedUser;

    public void setService(SuperService service) {
        this.service = service;
    }

    public void setLoggedUser(UserDTO loggedUser) {
        this.loggedUser = loggedUser;
    }

    public void loadEvents() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(LogInApplication.class.getResource("event-item.fxml"));
        AnchorPane pane = fxmlLoader.load();
        EventItemController eventItemController = fxmlLoader.getController();
        eventItemController.setService(service);
        eventItemController.setLoggedUser(loggedUser);
        pane.addEventHandler(EventEvent.ANY, this::handlerForNextPrevEvent);
        pane.addEventHandler(LoadEventsViews.LOAD_EVENTS, this::handlerForLoadEvents);
        this.currentEvent = 0;
        if (service.size() != 0) {
            eventItemController.load(service.getEventByPosition(currentEvent));
        }
        else {
            loadCreateEvent();
        }
        Pane itemEvent = new Pane(pane);
        borderPane.setCenter(itemEvent);
    }

    public void handlerShowEventBtn() throws IOException {
        if (service.size() != 0) {
            loadEvents();
        }
    }

    private void handlerForLoadEvents(Event e){
        if (service.size() == 0){
                loadCreateEvent();
        }
        else {
            try {
                loadEvents();
            } catch (IOException exception) {
                exception.printStackTrace();
            }
        }
    }

    private void handlerForNextPrevEvent(Event t){
        if (t.getEventType().equals(EventEvent.NEXT_EVENT)) {
            if ((service.size()!= 0) && (this.currentEvent < service.size()-1)) {
                    this.currentEvent++;
                    loadEventItem();
            }
            else if (service.size() == 0){
                    loadCreateEvent();
            }

        } else if (t.getEventType().equals(EventEvent.PREVIOUS_EVENT)){
            if ((service.size()!= 0) && (this.currentEvent > 0)) {
                    this.currentEvent--;
                    loadEventItem();
            }
            else if (service.size() == 0){
                    loadCreateEvent();
            }
        }
    }

    public void loadEventItem() {
        FXMLLoader fxmlLoader = new FXMLLoader(LogInApplication.class.getResource("event-item.fxml"));
        AnchorPane pane = null;
        try {
            pane = fxmlLoader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        pane.addEventHandler(EventEvent.ANY, this::handlerForNextPrevEvent);
        pane.addEventHandler(LoadEventsViews.LOAD_EVENTS, this::handlerForLoadEvents);
        EventItemController eventItemController = fxmlLoader.getController();
        eventItemController.setService(this.service);
        eventItemController.setLoggedUser(loggedUser);
        eventItemController.load(service.getEventByPosition(this.currentEvent));
        Pane view = new Pane(pane);
        borderPane.setCenter(view);
    }

    public void handlerCreateEventBtn() {
        loadCreateEvent();
    }

    public void loadCreateEvent() {
        FXMLLoader fxmlLoader = new FXMLLoader(LogInApplication.class.getResource("createEvent-view.fxml"));
        AnchorPane panel = null;
        try {
            panel = fxmlLoader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        CreateEventController controller = fxmlLoader.getController();
        controller.setLoggedUser(loggedUser);
        controller.setService(service);
        Pane view = new Pane(panel);
        borderPane.setCenter(view);
    }
}
