package com.example.proiect_extins.controller;

import com.example.proiect_extins.domain.EventDTO;
import com.example.proiect_extins.domain.UserDTO;
import com.example.proiect_extins.service.SuperService;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class CreateEventController {

    private SuperService superService;
    private UserDTO loggedUser;

    @FXML
    private DatePicker dateEvent;

    @FXML
    private TextField descriptionEvent;

    @FXML
    private TextField locationEvent;

    @FXML
    private TextField nameEvent;

    public void handlerAddEventBtn() {
        if ((descriptionEvent.getText().length() == 0) || (locationEvent.getText().length() == 0) || (dateEvent.getValue() == null) || nameEvent.getText().length() == 0){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText("All TextFields must be fill!");
            alert.show();
        }
        else if (dateEvent.getValue().isBefore(LocalDate.now())){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText("Invalid date!");
            alert.show();
        }

        else{
            List<UserDTO> list = new ArrayList<>();
            list.add(loggedUser);
            superService.save(new EventDTO(nameEvent.getText(), descriptionEvent.getText(), locationEvent.getText(), dateEvent.getValue(), list));
            descriptionEvent.clear();
            locationEvent.clear();
            nameEvent.clear();
            dateEvent.getEditor().clear();
        }
    }

    public void setService(SuperService superService) {
        this.superService = superService;
    }

    public void setLoggedUser(UserDTO loggedUser) {
        this.loggedUser = loggedUser;
    }

}
