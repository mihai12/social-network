package com.example.proiect_extins.controller;

import com.example.proiect_extins.LogInApplication;
import com.example.proiect_extins.domain.User;
import com.example.proiect_extins.domainEvent.ButtonClicked;
import com.example.proiect_extins.service.SuperService;
import com.example.proiect_extins.service.validators.ValidationException;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import org.w3c.dom.events.MouseEvent;

import java.io.IOException;

public class LogInController {

    @FXML
    private TextField usernameTxt;
    @FXML
    private PasswordField passwordTxt;
    @FXML
    private Button logInBtn;
    @FXML
    private AnchorPane mainPane;

    Stage signUpStage;

    private SuperService service;
    private double yCord;
    private double xCord;
    private MainWindowController mainWindowController;

    @FXML
    private void handlerSignUp() throws IOException{
        FXMLLoader loader = new FXMLLoader(LogInApplication.class.getResource("signUpView.fxml"));
        signUpStage = new Stage();
        AnchorPane anchorPane = loader.load();
        Scene scene = new Scene(anchorPane, 340, 360);
        SignUpViewController signUpViewController = loader.getController();
        signUpViewController.setService(service);
        signUpStage.setScene(scene);
        signUpStage.initStyle(StageStyle.TRANSPARENT);
        signUpStage.show();
        anchorPane.addEventFilter(ButtonClicked.ANY, this::handlerCloseSignUp);
    }

    public void handlerCloseSignUp(ButtonClicked clicked){
        signUpStage.close();
    }

    @FXML
    private void handlerLogIn() throws IOException {
        String username= usernameTxt.getText();
        String password= passwordTxt.getText();
        try {
            User user = service.logIn(username, password);

            FXMLLoader fxmlLoader = new FXMLLoader(LogInApplication.class.getResource("mainWindow.fxml"));
            Stage manWindowStage= new Stage();
            AnchorPane panel= fxmlLoader.load();

            MainWindowController mainWindowController= fxmlLoader.getController();
            mainWindowController.load(service,user);

            Scene scene = new Scene(panel, 700, 520);

            panel.setOnMousePressed(event->{
                xCord = event.getSceneX();
                yCord = event.getSceneY();
            });
            panel.setOnMouseDragged(event->{
                manWindowStage.setX(event.getScreenX() - xCord);
                manWindowStage.setY(event.getScreenY() - yCord);
            });

            scene.getStylesheets().add(LogInApplication.class.getResource("mainWindow.css").toExternalForm());
            scene.setFill(Color.TRANSPARENT);
            manWindowStage.setScene(scene);
            manWindowStage.initStyle(StageStyle.TRANSPARENT);
            manWindowStage.initModality(Modality.NONE);
            manWindowStage.show();

            Stage logInStage =(Stage) logInBtn.getScene().getWindow();
            logInStage.close();


        }catch (ValidationException e){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText(e.getMessage());
            alert.show();

        }
    }

    public void setService(SuperService service) {
        this.service = service;
    }

    @FXML
    public void handlerExitBtn(Event actionEvent) {
        ((Stage)(((ImageView)actionEvent.getSource()).getScene().getWindow())).close();
    }

    @FXML
    public void handlerMinimizeBtn(Event actionEvent) {
        ((Stage)(((ImageView)actionEvent.getSource()).getScene().getWindow())).setIconified(true);
    }

    @FXML
    public void handlerExtindButton(Event actionEvent) {
        ((Stage)(((ImageView)actionEvent.getSource()).getScene().getWindow())).setFullScreen(true);
    }

}
