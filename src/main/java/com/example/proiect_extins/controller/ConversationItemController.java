package com.example.proiect_extins.controller;

import com.example.proiect_extins.domainEvent.ItemSelected;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;

public class ConversationItemController {
    @FXML
    private AnchorPane anchorPaneConv;

    @FXML
    private Label nameLabel;

    @FXML
    private ImageView userImg;

    @FXML
    private ImageView groupImg;

    private boolean isGroup=false;

    /**
     * Set the name of the label.
     * @param name name of the label
     */
    public void setNameLabel(String name, boolean group)
    {
        if(group) {
            userImg.setVisible(false);
        }
        else{
            groupImg.setVisible(false);
        }
        nameLabel.setText(name);
    }

    /**
     * handle the clicks
     * raise a new ItemSelect Event with the id of the user or the group which is displayed(selected)
     */
    public void handlerMouseClick(MouseEvent mouseEvent) {
        if (isGroup)
            anchorPaneConv.fireEvent(new ItemSelected(ItemSelected.GROUP_LOAD_MSJ, nameLabel.getId()));
        else
        anchorPaneConv.fireEvent(new ItemSelected(ItemSelected.USER_LOAD_MSJ, nameLabel.getId()));
    }

    public void setIsGroup(boolean group) {
        isGroup = group;
    }
}
