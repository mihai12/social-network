package com.example.proiect_extins.controller;

import com.example.proiect_extins.domain.NotificationEventDTO;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;

public class NotificationItemController {
    @FXML
    private Label dateNotification;

    @FXML
    private Label locationNotification;

    @FXML
    private Label nameNotification;

    @FXML
    private Label noDays;

    public void setItem(NotificationEventDTO notification) {
        nameNotification.setText(notification.getName());
        dateNotification.setText(notification.getDate().toString());
        locationNotification.setText(notification.getLocation());
        noDays.setText(String.valueOf(notification.getPeriod()));
    }

}
