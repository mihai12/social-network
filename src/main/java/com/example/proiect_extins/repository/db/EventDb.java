package com.example.proiect_extins.repository.db;

import com.example.proiect_extins.domain.EventDTO;
import com.example.proiect_extins.domain.NotificationEventDTO;
import com.example.proiect_extins.domain.User;
import com.example.proiect_extins.domain.UserDTO;
import com.example.proiect_extins.repository.Repository;

import java.sql.*;
import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class EventDb implements Repository<Integer, EventDTO> {
    private String url;
    private String username;
    private String password;

    public EventDb(String url, String username, String password) {
        this.url = url;
        this.username = username;
        this.password = password;
    }

    @Override
    public EventDTO findOne(Integer id) {
        String sql = "select name, description, date, location, event_user.email, users.first_name, users.last_name from event inner join event_user on event.id = event_user.id_event inner join users on users.email = event_user.email where event.id = ?";
        List<UserDTO> list = new ArrayList<>();
        String name = null;
        EventDTO eventDTO = null;
        String description = null;
        String location = null;
        LocalDate date = null;
        try(Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                name = resultSet.getString("name");
                description = resultSet.getString("description");
                location = resultSet.getString("location");
                date = LocalDate.parse(resultSet.getString("date"));
                String first_name = resultSet.getString("first_name");
                String last_name = resultSet.getString("last_name");
                String email = resultSet.getString("email");
                UserDTO userDTO = new UserDTO(email, first_name, last_name);
                list.add(userDTO);
            }
            eventDTO = new EventDTO(name, description, location, date,  list);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return eventDTO;
    }

    @Override
    public Iterable<EventDTO> findAll() {
        List<EventDTO> eventDTOSet = new ArrayList<>();
        List<UserDTO> listUsers = new ArrayList<>();
        String sql = "select * from event order by date asc";
        String sqlUsers = "select users.first_name, users.last_name, users.email from users inner join event_user on users.email = event_user.email where event_user.id_event = ?";
        try(Connection connection = DriverManager.getConnection(url, username, password);
        PreparedStatement statement = connection.prepareStatement(sql);
        PreparedStatement statement1 = connection.prepareStatement(sqlUsers)){
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()){
                int id = resultSet.getInt("id");
                String name = resultSet.getString("name");
                String description = resultSet.getString("description");
                String location = resultSet.getString("location");
                LocalDate date = LocalDate.parse(resultSet.getString("date"));
                statement1.setInt(1, id);
                ResultSet resultSet1 = statement1.executeQuery();
                while(resultSet1.next()) {
                    String lastName = resultSet1.getString("last_name");
                    String email = resultSet1.getString("email");
                    String firstName = resultSet1.getString("first_name");
                    UserDTO userDTO = new UserDTO(email, firstName, lastName);
                    listUsers.add(userDTO);
                }
                EventDTO eventDTO = new EventDTO(name, description, location, date, listUsers);
                eventDTO.setId(id);
                eventDTOSet.add(eventDTO);
                listUsers.clear();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return eventDTOSet;
    }

    @Override
    public EventDTO save(EventDTO entity) {
        String sqlEvent = "insert into event(name, description, location, date) values (?, ?, ?, ?) returning id";
        String sqlEventUser = "insert into event_user(id_event, email) values (?, ?)";
        try(Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement statement = connection.prepareStatement(sqlEvent);
            PreparedStatement statement1 = connection.prepareStatement(sqlEventUser)){
                statement.setString(1, entity.getName());
                statement.setString(2, entity.getDescription());
                statement.setString(3, entity.getLocation());
                statement.setDate(4, Date.valueOf(entity.getDate()));
                ResultSet resultSet = statement.executeQuery();
                resultSet.next();
                int id = resultSet.getInt(1);
                entity.setId(id);
                for(UserDTO userDTO:entity.getParticipants()){
                    statement1.setInt(1, id);
                    statement1.setString(2, userDTO.getId());
                    statement1.executeUpdate();
                }
                return entity;
        }catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public EventDTO remove(Integer integer) {
        String sqlDeleteEventUser = "delete from event_user where id_event=?";
        String sqlDeleteEvent = "delete from event where id = ?";
        try(Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement statement = connection.prepareStatement(sqlDeleteEventUser);
            PreparedStatement statement1 = connection.prepareStatement(sqlDeleteEvent)){
            statement.setInt(1, integer);
            statement.executeUpdate();
            statement1.setInt(1, integer);
            statement1.executeUpdate();
        }catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public int size() {
        int size = 0;
        String sql = "select count (*) from event";
        try(Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement statement = connection.prepareStatement(sql)){
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            size = resultSet.getInt(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return size;
    }

    public UserDTO addParticipant(UserDTO user, int eventId){
        String sql = "insert into event_user(id_event, email) values (?, ?)";
        try(Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement preparedStatement = connection.prepareStatement(sql)){
            preparedStatement.setInt(1, eventId);
            preparedStatement.setString(2, user.getId());
            preparedStatement.executeUpdate();
            return user;
        }catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }

    public void removeParticipant(String email, int eventId){
        String sql = "delete from event_user where id_event = ? and email = ?";
        try(Connection connection = DriverManager.getConnection(url, username, password);
        PreparedStatement preparedStatement = connection.prepareStatement(sql)){
            preparedStatement.setInt(1, eventId);
            preparedStatement.setString(2, email);
            preparedStatement.executeUpdate();
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    public Integer noParticipants(int eventId) {
        String sql = "select count (*) as nr from event_user where id_event=?";
        int id=0;
        try(Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement statement = connection.prepareStatement(sql)){
            statement.setInt(1, eventId);
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            id = resultSet.getInt("nr");
        }catch (SQLException e){
            e.printStackTrace();
        }
        return id;
    }

    public boolean userParticipate(String id, Integer id1) {
        String sql = "select * from event_user where id_event =? and email =?";
        try(Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement statement = connection.prepareStatement(sql)){
            statement.setInt(1, id1);
            statement.setString(2, id);
            ResultSet resultSet = statement.executeQuery();
            if(resultSet.next()){
                return true;
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return false;
    }

    public void turnOnNotificationEvent(String id, Integer id1) {
        String sql = "update event_user set notify = true where id_event = ? and email = ?";
        try(Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement statement = connection.prepareStatement(sql)){
            statement.setInt(1, id1);
            statement.setString(2, id);
            statement.executeUpdate();
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    public void turnOffNotificationEvent(String id, Integer id1) {
        String sql = "update event_user set notify = false where id_event = ? and email = ?";
        try(Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement statement = connection.prepareStatement(sql)){
            statement.setInt(1, id1);
            statement.setString(2, id);
            statement.executeUpdate();
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    public boolean getStatusNotifications(String id, Integer id1) {
        boolean ok = false;
        String sql = "select notify from event_user where id_event = ? and email = ?";
        try(Connection connection = DriverManager.getConnection(url, username, password);
        PreparedStatement statement = connection.prepareStatement(sql)){
            statement.setInt(1, id1);
            statement.setString(2, id);
            ResultSet resultSet = statement.executeQuery();
            if(resultSet.next()) {
                ok = resultSet.getBoolean("notify");
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return ok;
    }

    public List<NotificationEventDTO> getNotificationsEvents(String id, LocalDate now) {
        List<NotificationEventDTO> notifications = new ArrayList<>();
        String sql = "select * from event inner join event_user on event_user.id_event = event.id where event_user.email = ? and event_user.notify = true";
        try(Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement statement = connection.prepareStatement(sql)){
            statement.setString(1, id);
            ResultSet resultSet = statement.executeQuery();
            while(resultSet.next()){
                int idEvent = resultSet.getInt("id");
                String name = resultSet.getString("name");
                String location = resultSet.getString("location");
                String description = resultSet.getString("description");
                LocalDate date = resultSet.getDate("date").toLocalDate();
                Period period = Period.between(now, date);
                if(period.getDays() <= 2){
                    NotificationEventDTO notification = new NotificationEventDTO(idEvent, name, location, description, date, period.getDays());
                    notifications.add(notification);
                }
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return notifications;
    }

    public EventDTO getEventByPosition(int currentEvent) {
        List<UserDTO> listUsers = new ArrayList<>();
        String sql = "select * from event order by date asc limit 1 offset ?";
        String sqlUsers = "select users.first_name, users.last_name, users.email from users inner join event_user on users.email = event_user.email where event_user.id_event = ?";
        try(Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement statement = connection.prepareStatement(sql);
            PreparedStatement statement1 = connection.prepareStatement(sqlUsers)){
            statement.setInt(1, currentEvent);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()){
                int id = resultSet.getInt("id");
                String name = resultSet.getString("name");
                String description = resultSet.getString("description");
                String location = resultSet.getString("location");
                LocalDate date = LocalDate.parse(resultSet.getString("date"));
                statement1.setInt(1, id);
                ResultSet resultSet1 = statement1.executeQuery();
                while(resultSet1.next()) {
                    String lastName = resultSet1.getString("last_name");
                    String email = resultSet1.getString("email");
                    String firstName = resultSet1.getString("first_name");
                    UserDTO userDTO = new UserDTO(email, firstName, lastName);
                    listUsers.add(userDTO);
                }
                EventDTO eventDTO = new EventDTO(name, description, location, date, listUsers);
                eventDTO.setId(id);
                return eventDTO;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
