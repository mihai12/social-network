module com.example.proiect_extins {
    requires javafx.controls;
    requires javafx.fxml;

    requires org.controlsfx.controls;
    requires com.dlsc.formsfx;
    requires org.kordamp.bootstrapfx.core;
    requires java.sql;
    requires org.apache.pdfbox;

    opens com.example.proiect_extins to javafx.fxml;
    exports com.example.proiect_extins;

    exports com.example.proiect_extins.controller;
    opens com.example.proiect_extins.controller to javafx.fxml;
}